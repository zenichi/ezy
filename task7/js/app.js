
$(document).ready(function () {
    Module.addHeaderContent();
    Module.setClock();
    Module.alertUserAboutTimeSpend();
});

var Module = (function() {

    //private methods
    function _alertMe(i) {
        if (i == 5) {
            toastr.info("five");
        } else
            toastr.info(i);
    }

    function _tooLong() {
        toastr.info('you have now been on the page for half a minute');
    }

    //public methods
    function loadList() {
        $(".THELIST").empty();

        for (var i = 0; i < 10; i++) {
            newElem = $("<li><a href='#'>" + i + "</a></li>");
            newElem.click(function() {
                _alertMe($(this).children().eq(0).text());
            });
            $(".THELIST").append(newElem);
        }
    }

    function alertUserAboutTimeSpend() {
        setTimeout(_tooLong, 30000);
    }

    function setClock() {
        var d = new Date();
        $("#Clock").text(d.getHours() + ':' + d.getMinutes());
    }

    function addHeaderContent() {
        $('#header').prepend('<h1>The fantastic <small>javascript example</small></h1>');
    }

    return {
        loadList: loadList,
        alertUserAboutTimeSpend: alertUserAboutTimeSpend,
        setClock: setClock,
        addHeaderContent: addHeaderContent
    };

})();





