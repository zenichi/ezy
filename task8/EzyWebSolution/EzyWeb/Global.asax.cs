﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using EzyWeb.Contracts;
using EzyWeb.Services;
using Microsoft.Practices.Unity;
using Unity.WebApi;

namespace EzyWeb
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var unityContainer = new UnityContainer();

            unityContainer.RegisterType<ICurrencyService, CurrencyService>();
            unityContainer.RegisterType<ICurrencyMapper, CurrencyMapper>();
            unityContainer.RegisterType<IForexWrapper, ForexWrapper>();
            unityContainer.RegisterType<IEzySerializer, EzySerializer>();

            GlobalConfiguration.Configuration.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(unityContainer);
        }
    }
}
