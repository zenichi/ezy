﻿using System.Net;

namespace EzyWeb.Contracts
{
    public interface IForexWrapper
    {
        HttpWebResponse ExecuteRequest(string requestUrl);
    }
}