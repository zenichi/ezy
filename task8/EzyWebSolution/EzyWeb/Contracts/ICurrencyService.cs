﻿using System.Collections.Generic;
using EzyWeb.Models;

namespace EzyWeb.Contracts
{
    public interface ICurrencyService
    {
        IEnumerable<CurrencyRateViewModel> GetRates();
    }
}