﻿using System.Collections.Generic;
using EzyWeb.Models;
using EzyWeb.Models.Forex;

namespace EzyWeb.Contracts
{
    public interface ICurrencyMapper
    {
        IEnumerable<CurrencyRateViewModel> Map(IEnumerable<Row> input);
    }
}