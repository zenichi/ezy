﻿using System.Net;

namespace EzyWeb.Contracts
{
    public interface IEzySerializer
    {
        T Deserialize<T>(HttpWebResponse ratesResponse);
    }
}