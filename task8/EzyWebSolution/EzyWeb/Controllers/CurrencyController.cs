﻿using System.Collections.Generic;
using System.Web.Http;
using EzyWeb.Contracts;
using EzyWeb.Models;

namespace EzyWeb.Controllers
{
    [RoutePrefix("api/currency")]
    public class CurrencyController : ApiController
    {
        private readonly ICurrencyService _currencyService;

        public CurrencyController(ICurrencyService currencyService)
        {
            _currencyService = currencyService;
        }

        [HttpGet]
        [Route("rates")]
        public IEnumerable<CurrencyRateViewModel> Get()
        {
            var rates = _currencyService.GetRates();
            return rates;
        }
    }
}