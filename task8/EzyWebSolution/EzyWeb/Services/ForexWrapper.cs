using System;
using System.Net;
using EzyWeb.Contracts;

namespace EzyWeb.Services
{
    public class ForexWrapper : IForexWrapper
    {
        public HttpWebResponse ExecuteRequest(string requestUrl)
        {
            try
            {
                var request = WebRequest.Create(requestUrl) as HttpWebRequest;

                var response = request?.GetResponse() as HttpWebResponse;
                if (response == null || response.StatusCode != HttpStatusCode.OK)
                {
                    //todo handle;
                    throw new Exception("Wrong response from server!");
                }

                return response;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                // todo: log/handle exception
                throw;
            }
        }
    }
}