﻿using System.Net;
using System.Xml.Serialization;
using EzyWeb.Contracts;
using EzyWeb.Models.Forex;

namespace EzyWeb.Services
{
    public class EzySerializer : IEzySerializer
    {
        public T Deserialize<T>(HttpWebResponse response)
        {
            XmlSerializer serializer = new XmlSerializer(typeof (Web_dis_rates));
            var stream = response.GetResponseStream();
            if (stream != null)
            {
                var res = (T) serializer.Deserialize(stream);

                return res;
            }

            return default(T);
        }
    }
}