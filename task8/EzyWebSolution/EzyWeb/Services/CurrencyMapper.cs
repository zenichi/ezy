﻿using System.Collections.Generic;
using EzyWeb.Contracts;
using EzyWeb.Models;
using EzyWeb.Models.Forex;

namespace EzyWeb.Services
{
    public class CurrencyMapper : ICurrencyMapper
    {
        public IEnumerable<CurrencyRateViewModel> Map(IEnumerable<Row> rows)
        {
            var result = new List<CurrencyRateViewModel>();
            if (rows == null)
                return result;

            foreach (var row in rows)
            {
                var buyAmount = 0m;
                if (!decimal.TryParse(row.Buy_cash, out buyAmount))
                {
                    // todo: do something;
                }
                var sellAmount = 0m;
                if (!decimal.TryParse(row.Sell_cash, out sellAmount))
                {
                    // todo: do something;
                }

                result.Add(new CurrencyRateViewModel
                {
                    Base = row.Base_swift,
                    Code = row.Swift_code,
                    Name = row.Swift_name,
                    BuyAmount = buyAmount,
                    SellAmount = sellAmount
                });
            }

            return result;
        }
    }
}