﻿using System.Collections.Generic;
using System.Linq;
using EzyWeb.Contracts;
using EzyWeb.Models;
using EzyWeb.Models.Forex;

namespace EzyWeb.Services
{
    public class CurrencyService : ICurrencyService
    {
        private readonly ICurrencyMapper _currencyMapper;
        private readonly IEzySerializer _ezySerializer;
        private readonly IForexWrapper _forexWrapper;

        public CurrencyService(ICurrencyMapper currencyMapper, IEzySerializer ezySerializer, IForexWrapper forexWrapper)
        {
            _currencyMapper = currencyMapper;
            _ezySerializer = ezySerializer;
            _forexWrapper = forexWrapper;
        }

        public IEnumerable<CurrencyRateViewModel> GetRates()
        {
            var ratesRequest = CreateRequest();
            var ratesResponse = _forexWrapper.ExecuteRequest(ratesRequest);

            var results = _ezySerializer.Deserialize<Web_dis_rates>(ratesResponse);
            if (results == null || !results.Row.Any())
            {
                return new List<CurrencyRateViewModel>();
            }

            var filteredRows = results.Row.Where(p => p.Swift_code.ToUpper() == "USD" || p.Swift_code.ToUpper() == "EUR");

            return _currencyMapper.Map(filteredRows);
        }

        public static string CreateRequest()
        {
            return "https://www.forex.se/ratesxml.asp?id=492";
        }
    }
}