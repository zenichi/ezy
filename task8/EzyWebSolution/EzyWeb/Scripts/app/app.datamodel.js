﻿function AppDataModel() {
    var self = this;
    // Routes
    self.getRatesUrl = "/api/currency/rates";
    self.siteUrl = "/";

    // Route operations

    // Other private operations

    // Operations

    // Data
    self.rates = ko.observableArray([]);
}