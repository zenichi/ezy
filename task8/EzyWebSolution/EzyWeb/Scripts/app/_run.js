﻿$(function () {
    app.initialize();

    // Activate Knockout
    ko.applyBindings(app);
});

var app = new AppViewModel(new AppDataModel());