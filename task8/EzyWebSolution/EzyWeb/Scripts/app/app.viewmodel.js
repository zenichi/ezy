﻿function AppViewModel(dataModel) {
    // Private state
    var self = this;

    self.data = dataModel;

    // The current item will be passed as the first parameter, so we know which place to remove
    self.getRates = function () {
        $.ajax({
            method: 'get',
            url: app.data.getRatesUrl,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                self.data.rates.removeAll();
                console.debug('SUCCESS: get rates: ' + data);
                ko.utils.arrayPushAll(self.data.rates, data);
            },
            error: function(data) {
                console.error('ERROR: get rates' + data);
            }
        });
    }

    self.initialize = function () {
        // we could get rates on page load here
    }
}