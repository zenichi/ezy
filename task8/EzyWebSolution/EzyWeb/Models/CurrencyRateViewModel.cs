﻿namespace EzyWeb.Models
{
    public class CurrencyRateViewModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public decimal BuyAmount { get; set; }
        public decimal SellAmount { get; set; }
        public string Base { get; set; }
    }
}