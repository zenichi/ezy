﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace EzyWeb.Models.Forex
{
    [XmlRoot(ElementName = "web_dis_rates")]
    public class Web_dis_rates
    {
        [XmlElement(ElementName = "timestamp")]
        public Timestamp Timestamp { get; set; }

        [XmlElement(ElementName = "row")]
        public List<Row> Row { get; set; }

        [XmlElement(ElementName = "copyright")]
        public string Copyright { get; set; }
    }
}