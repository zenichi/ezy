﻿using System.Xml.Serialization;

namespace EzyWeb.Models.Forex
{
    public class CurrencyGuide
    {
        [XmlAttribute(AttributeName = "BaseSwift")]
        public string BaseSwift { get; set; }

        [XmlAttribute(AttributeName = "Swift")]
        public string Swift { get; set; }

        [XmlAttribute(AttributeName = "Amount")]
        public string Amount { get; set; }

        [XmlText]
        public string Text { get; set; }
    }
}