﻿using System.Xml.Serialization;

namespace EzyWeb.Models.Forex
{
    [XmlRoot(ElementName = "timestamp")]
    public class Timestamp
    {
        [XmlAttribute(AttributeName = "Desc")]
        public string Desc { get; set; }

        [XmlText]
        public string Text { get; set; }
    }
}